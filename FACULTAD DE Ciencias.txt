FACULTAD DE Ciencias                                CODIGO   NEM RANK  LENG  MAT  HIST CS  POND  PSU   MAX    MIN	      PSU  BEA
Ingeniería en Estadística 							19072	 10	  40   10    30   10 ó 10  500   475   671.80 506.80      30   5
Lic. en Ciencias mención: Biología o Química 		19075 	 10	  40   20    20   -    10  500   475   735.50 538.10      25   2
Lic. en Física mención: Astronomía, Ciencias
Atmosféricas o Computación Científica				19078	 10	  20   15    45   -    10  500   500   659.65 510.00      45   5
Licenciatura en Matemáticas 						19070	 10	  40   10    30   10 ó 10  500   475   632.20 506.10      15   2
